#
# Be sure to run `pod lib lint DuomPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DuomPlayer'
  s.version          = '0.0.1'
  s.summary          = 'Duom&KiKi 播放器组件'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/gwave_mobile_public/duomvideoplayer'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'kuroky' => 'kai.he@duom.com' }
  s.source           = { :git => 'https://gitlab.com/gwave_mobile_public/duomvideoplayer.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '12.0'
  
  s.source_files = 'DuomPlayer/DuomPlayer.h'
  s.resources    = 'DuomPlayer/Assets/*'
  
  s.prefix_header_contents =  '#import "DuomPlayer.h"'
  
  s.subspec 'Player' do |spec|
      spec.source_files = 'DuomPlayer/Classes/Player/**/*.{h,m}'
  end
  
  s.subspec 'Views' do |spec|
      spec.source_files = 'DuomPlayer/Classes/Views/**/*.{h,m}'
  end
  
  s.subspec 'Models' do |spec|
      spec.source_files = 'DuomPlayer/Classes/Models/**/*.{h,m}'
  end
  
  s.subspec 'Category' do |spec|
      spec.source_files = 'DuomPlayer/Classes/Category/**/*.{h,m}'
  end
  
  s.subspec 'Untils' do |spec|
      spec.source_files = 'DuomPlayer/Classes/Untils/**/*.{h,m}'
  end
  
  s.dependency 'Masonry'
  s.dependency 'TXLiteAVSDK_Player', '10.6.11821'
  
end
