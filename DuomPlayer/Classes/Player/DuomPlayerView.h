//
//  DuomPlayerView.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/6.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DuomPlayer.h"
#import "DuomPlayerViewConfig.h"
#import "DuomPlayerModel.h"

@class TXVodPlayer;
@class TXImageSprite;
@class DuomPlayerView;
@class DuomPlayerControlView;

@protocol DuomPlayerPlayListener <NSObject>
@optional
/// 点播事件通知
/// @param player 点播播放器
/// @param evtID 参见TXLiveSDKTypeDef.h
/// @param param 参见TXLiveSDKTypeDef.h
- (void)onVodPlayEvent:(TXVodPlayer *)player event:(int)evtID withParam:(NSDictionary *)param;

/// 点播网络状态通知
/// @param player 点播播放器
/// @param param 参见TXLiveSDKTypeDef.h
- (void)onVodNetStatus:(TXVodPlayer *)player withParam:(NSDictionary *)param;

@end

@protocol DuomPlayerDelegate <NSObject>
@optional
/// 返回事件
- (void)duomPlayerBackAction:(DuomPlayerView *)player;
/// 全屏改变通知
- (void)duomPlayerFullScreenChanged:(DuomPlayerView *)player;
/// 播放开始通知
- (void)duomPlayerDidStart:(DuomPlayerView *)player;
/// 播放结束通知
- (void)duomPlayerDidEnd:(DuomPlayerView *)player;
/// 播放错误通知
- (void)duomPlayerError:(DuomPlayerView *)player errCode:(int)code errMessage:(NSString *)why;
// 需要通知到父view的事件在此添加
/// 轻拍事件回调
- (void)singleTapClick;
@end

/// 播放器的状态
typedef NS_ENUM(NSInteger, DuomPlayerState) {
    StateFailed,     // 播放失败
    StateBuffering,  // 缓冲中
    StatePrepare,    // 准备就绪
    StatePlaying,    // 播放中
    StateStopped,    // 停止播放
    StatePause,      // 暂停播放
    StateFirstFrame, // 第一帧画面
};

/// 播放器布局样式
typedef NS_ENUM(NSInteger, DuomPlayerLayoutStyle) {
    DuomPlayerLayoutStyleCompact,    ///< 精简模式
    DuomPlayerLayoutStyleFullScreen  ///< 全屏模式
};

@interface DuomPlayerView : UIView

/** 设置代理 */
@property(nonatomic, weak) id<DuomPlayerDelegate> delegate;

@property(nonatomic, weak) id<DuomPlayerPlayListener> playListener;

@property(nonatomic, assign) DuomPlayerLayoutStyle layoutStyle;

/// 设置播放器的父view。播放过程中调用可实现播放窗口转移
@property(nonatomic, weak) UIView *fatherView;

/// 播放器的状态
@property(nonatomic, assign) DuomPlayerState state;

/// 是否全屏
@property(nonatomic, assign, setter=setFullScreen:) BOOL isFullScreen;

/// 是否锁定旋转
@property(nonatomic, assign) BOOL isLockScreen;

/// 是否自动播放（在playWithModel前设置)
@property(nonatomic, assign) BOOL autoPlay;

/// 播放器控制层
@property(nonatomic, strong) DuomPlayerControlView *controlView;

/// 是否允许竖屏手势
@property(nonatomic, assign) BOOL disableGesture;

/// 是否在手势中
@property(nonatomic, assign, readonly) BOOL isDragging;

/// 是否加载成功
@property(nonatomic, assign, readonly) BOOL isLoaded;

/// 是否允许音量按钮控制，默认是不允许
@property(nonatomic, assign) BOOL disableVolumControl;

/// 是否开启画中画功能
/*
 画中画（PictureInPicture）在 iOS 9就已经推出了，不过之前都只能在 iPad 上使用，iPhone 要使用画中画需更新到 iOS 14才能使用。
 目前腾讯云播放器可以支持应用内和应用外画中画能力，极大的满足用户的诉求。使用前需要开通后台模式，步骤为：XCode 选择对应的 Target -> Signing & Capabilities -> Background Modes，勾选“Audio, AirPlay, and Picture in Picture”。
 */
@property(nonatomic, assign) BOOL disablePip;

/// 封面图片
@property(nonatomic, strong) UIImageView *coverImageView;

/// 重播按钮
@property(nonatomic, strong) UIButton *repeatBtn;

/// 播放按钮
@property(nonatomic, strong) UIButton *centerPlayBtn;

/// 全屏退出
@property(nonatomic, strong) UIButton *repeatBackBtn;

/// 视频总时长
@property(nonatomic, assign) CGFloat playDuration;

/// 视频当前播放时间
@property(nonatomic, assign) CGFloat playCurrentTime;

/// 起始播放时间，用于从上次位置开播
@property(nonatomic, assign) CGFloat startTime;

/// 播放的视频Model
@property(nonatomic, assign, readonly) DuomPlayerModel *playerModel;

/// 播放器配置
@property(nonatomic, strong) DuomPlayerViewConfig *playerConfig;

/// 循环播放
@property(nonatomic, assign) BOOL loop;

/**
 * 播放model
 */
- (void)playWithModel:(DuomPlayerModel *)playerModel;

/**
 * 播放一组视频
 *
 *  @param playModelList    视频模型数组
 *  @param isLoop    是否循环播放
 *  @param index   起始位置
 */
- (void)playWithModelList:(NSArray *)playModelList isLoopPlayList:(BOOL)isLoop startIndex:(NSInteger)index;

/**
 * 重置player
 */
- (void)resetPlayer;

/**
 * 播放
 */
- (void)resume;

/**
 * 暂停
 * @warn isLoaded == NO 时暂停无效
 */
- (void)pause;

/**
 * 停止播放
 */
- (void)removeVideo;

/**
 *  从xx秒开始播放视频跳转
 *
 *  @param dragedSeconds 视频跳转的秒数
 */
- (void)seekToTime:(NSInteger)dragedSeconds;

/**
 *  是否显示视频左上方的返回按钮
 *
 *  @param isShow 是否显示
 */
- (void)showOrHideBackBtn:(BOOL)isShow;




@end
