//
//  DuomPlayerView+ScreenCaptured.h
//  DuomPlayer
//
//  Created by kuroky on 2022/11/7.
//

#import "DuomPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DuomPlayerView (ScreenCaptured)

@property(nonatomic, strong) UIAlertController *alert;

- (void)addScreenCapturedNotification;

@end

NS_ASSUME_NONNULL_END
