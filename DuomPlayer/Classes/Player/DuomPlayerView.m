//
//  DuomPlayerView.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/6.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomPlayerView.h"
#import <AVFoundation/AVFoundation.h>
#import <Photos/Photos.h>

#import "DuomPlayerControlView.h"
#import "TXPlayerGlobalSetting.h"
#import "DuomPlayer.h"
#import "DuomPlayerView+Private.h"
#import "TXCUrl.h"
#import "UIView+Fade.h"
#import "UIView+MMLayout.h"
#import "StrUtils.h"
#import "DuomDefaultControlView.h"
#import "DuomVideoFrameDescription.h"
#import "DuomPlayerView+ActiveNotification.h"
#import "DuomPlayerView+Gesture.h"
#import "DuomPlayerView+ScreenCaptured.h"

#import "TXBitrateItemHelper.h"
#import "TXVideoCustomProcessDelegate.h"
#import "TXVodPlayConfig.h"
#import "TXVodPlayListener.h"
#import "TXVodPlayer.h"
#ifdef ENABLE_UGC
#import "TXUGCBase.h"
#import "TXUGCPartsManager.h"
#import "TXUGCRecord.h"
#import "TXUGCRecordListener.h"
#import "TXUGCRecordTypeDef.h"
#endif
#import "TXAudioCustomProcessDelegate.h"
#import "TXAudioRawDataDelegate.h"
#import "TXBitrateItem.h"
#import "TXImageSprite.h"
#import "TXLiteAVCode.h"
#import "TXLiveAudioSessionDelegate.h"
#import "TXLiveBase.h"
#import "TXLivePlayConfig.h"
#import "TXLivePlayListener.h"
#import "TXLivePlayer.h"
#import "TXLiveRecordListener.h"
#import "TXLiveRecordTypeDef.h"
#import "TXLiveSDKEventDef.h"
#import "TXLiveSDKTypeDef.h"
#import "TXPlayerAuthParams.h"


static UISlider *_volumeSlider;

@interface DuomPlayerView()<TXVodPlayListener>

@property (nonatomic, strong) UIActivityIndicatorView *pipLoadingView;

@end

@implementation DuomPlayerView {
    DuomPlayerControlView   *_controlView;
    NSURLSessionTask *      _currentLoadingTask;
    NSString               *_currentVideoUrl;
    BOOL                   _isPrepare;
    BOOL                   _restoreUI;
    BOOL                   _hasStartPipLoading;

    NSInteger              _playingIndex;
    BOOL                   _isLoopPlayList;
    BOOL                   _isVideoList;
    NSArray                *_videoModelList;
}

#pragma mark -- life Cycle

+ (void)initialize {
    NSString *path = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *cachePath = [path stringByAppendingString:@"/TXCache"];
    [TXPlayerGlobalSetting setCacheFolderPath:cachePath];
    [TXPlayerGlobalSetting setMaxCacheSize:800];
}

/**
 *  代码初始化调用此方法
 */
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeThePlayer];
    }
    return self;
}

/**
 *  初始化player
 */
- (void)initializeThePlayer {
    LOG_ME;
    self.netWatcher = [[NetWatcher alloc] init];

    CGRect frame    = CGRectMake(0, -100, 10, 0);
    self.volumeView = [[MPVolumeView alloc] initWithFrame:frame];
    [self.volumeView sizeToFit];
    for (UIWindow *window in [[UIApplication sharedApplication] windows]) {
        if (!window.isHidden) {
            [window addSubview:self.volumeView];
            break;
        }
    }

    _fullScreenBlackView                 = [UIView new];
    _fullScreenBlackView.backgroundColor = [UIColor blackColor];

    // 单例slider
    _volumeSlider = nil;
    for (UIView *view in [self.volumeView subviews]) {
        if ([view.class.description isEqualToString:@"MPVolumeSlider"]) {
            _volumeSlider = (UISlider *)view;
            break;
        }
    }

    _playerConfig = [[DuomPlayerViewConfig alloc] init];
    
    __weak __typeof(self) weakSelf = self;
    DuomPlayerWindowShared.closeHandler = ^{
        __strong __typeof(weakSelf) self = weakSelf;
        [DuomPlayerWindowShared hide];
        [self resetPlayer];
        DuomPlayerWindowShared.backController = nil;
    };
    
    // 添加通知
    [self addNotifications];
    // 添加手势
    [self createGesture];
    
    self.autoPlay = YES;
    _hasStartPip = NO;
    _restoreUI = NO;
    _hasStartPipLoading = NO;
}

- (void)dealloc {
    LOG_ME;
    // 移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];

    [self.netWatcher stopWatch];
    [self.volumeView removeFromSuperview];
}


#pragma mark - Public Method
- (void)playWithModelList:(NSArray *)playModelList isLoopPlayList:(BOOL)isLoop startIndex:(NSInteger)index {
    if (playModelList.count <= 0) {
        return;
    }
    _videoModelList = playModelList;
    _playingIndex = index;
    _isLoopPlayList = isLoop;
    _isVideoList = YES;
    [self.vodPlayer stopPlay];
    [self playModelInList:index];
}


- (void)playWithModel:(DuomPlayerModel *)playerModel {
    LOG_ME;
    _videoModelList = nil;
    _playerModel = playerModel;
    _isVideoList = NO;
    [self.controlView setNextBtnState:NO];
    [self setChildViewState];
    [self _playWithModel:playerModel];
}


- (void)_playWithModel:(DuomPlayerModel *)playerModel {
    [_currentLoadingTask cancel];
    _currentLoadingTask = nil;
    
    NSString *videoURL = playerModel.playingDefinitionUrl;
    if (videoURL != nil) {
        [self configTXPlayer];
    } else {
        NSLog(@"无播放地址");
        return;
    }
}

- (void)reloadModel {
    DuomPlayerModel *model = _playerModel;
    if (model) {
        [self resetPlayer];
        [self _playWithModel:_playerModel];
        [self addNotifications];
    }
}

- (void)_onModelLoadSucceed:(DuomPlayerModel *)model {
    if (model == _playerModel) {
        [self _playWithModel:_playerModel];
    }
}

- (void)_onModelLoadFailed:(DuomPlayerModel *)model error:(NSError *)error {
    if (model != _playerModel) {
        return;
    }
    // error 错误信息
    [self showMiddleBtnMsg:kStrLoadFaildRetry withAction:ActionRetry];
    [self.spinner stopAnimating];
    NSLog(@"Load play model failed error: %@", error);
    if ([self.delegate respondsToSelector:@selector(duomPlayerError:errCode:errMessage:)]) {
        NSString *message = [NSString stringWithFormat:@"网络请求失败 %d %@", (int)error.code, error.localizedDescription];
        [self.delegate duomPlayerError:self errCode:(int)error.code errMessage:message];
    }
    return;
}

/**
 *  重置player
 */
- (void)resetPlayer {
    LOG_ME;
    // 移除通知
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // 暂停
    [self pause];

    [self.vodPlayer stopPlay];
    [self.vodPlayer removeVideoWidget];
    self.vodPlayer = nil;

    [self.livePlayer stopPlay];
    [self.livePlayer removeVideoWidget];
    self.livePlayer = nil;

    self.state = StateStopped;
}

/**
 *  播放
 */
- (void)resume {
    LOG_ME;
    [self.controlView setPlayState:YES];
    self.isPauseByUser = NO;
    self.centerPlayBtn.hidden = YES;
    [self.controlView setSliderState:YES];
    [self.controlView setTopViewState:YES];
    if (_playerModel.action == PLAY_ACTION_MANUAL_PLAY) {
        [self showOrHideBackBtn:YES];
    }
    
    if (self.isLive) {
        self.state         = StatePlaying;
        [_livePlayer resume];
    } else {
        if (self.state == StatePause || self.state == StateBuffering) {
            [self.vodPlayer resume];
            self.state = StatePlaying;
            if (self.delegate && [self.delegate respondsToSelector:@selector(duomPlayerDidStart:)]) {
                [self.delegate duomPlayerDidStart:self];
            }
        } else if (self.state == StatePlaying) {
            [self.spinner stopAnimating];
        } else {
            if (self.state == StatePrepare) {
                self.state         = StatePlaying;
                self.coverImageView.hidden = YES;
                [self.vodPlayer resume];
            } else {
                self.centerPlayBtn.hidden = NO;
                _isPrepare = YES;
            }
        }
    }
}

/**
 * 暂停
 */
- (void)pause {
    LOG_ME;
    if (!self.isLoaded) return;
    if (self.playDidEnd) return;
    self.repeatBtn.hidden     = YES;
    [[self.controlView fadeShow] fadeOut:1];
    [self.controlView setPlayState:NO];
    self.isPauseByUser = YES;
    self.state         = StatePause;
    self.centerPlayBtn.hidden = NO;
    if (self.isLive) {
        [_livePlayer pause];
    } else {
        [self.vodPlayer pause];
    }
}

/**
 * 停止播放
 */
- (void)removeVideo {
    _isPrepare = NO;
    self.state = StateStopped;
    [self.vodPlayer stopPlay];
    [self.vodPlayer removeVideoWidget];
}

/**
 * 是否显示视频左上方的返回按钮
 */
- (void)showOrHideBackBtn:(BOOL)isShow {
    [self.controlView showOrHideBackBtn:isShow];
}

/**
 * seek
 */
- (void)seekToTime:(NSInteger)dragedSeconds {
    if (!self.isLoaded || self.state == StateStopped) {
        return;
    }
    if (self.isLive) {
        int ret = [self.livePlayer seek:dragedSeconds];
        if (ret != 0) {
            [self showMiddleBtnMsg:kStrTimeShiftFailed withAction:ActionNone];
            [self.middleBlackBtn fadeOut:2];
            [self resetControlViewWithLive:self.isLive shiftPlayback:self.isShiftPlayback isPlaying:YES];
        } else {
            if (!self.isShiftPlayback) self.isLoaded = NO;
            self.isShiftPlayback = YES;
            self.state           = StateBuffering;
            [self resetControlViewWithLive:YES shiftPlayback:self.isShiftPlayback isPlaying:YES];  //时移播放不能切码率
        }
    } else {
        if (!_vodPlayer) {
            [self setVodPlayConfig];
            [self restart];
        } else {
            [self.vodPlayer resume];
            [self.vodPlayer seek:dragedSeconds];
            [self.controlView setPlayState:YES];
        }
    }
}

#pragma mark - Private Method
- (void)addNotifications {
    [self addActiveNotifications];
    [self addDeviceOrientationNotifications];
    [self addScreenCapturedNotification];
}
- (void)playModelInList:(NSInteger)index {
    if (index < 0) {
        return;
    }
    
    if (index >= _videoModelList.count) {
        return;
    }
    
    _playerModel = _videoModelList[index];
    [self.controlView setNextBtnState:YES];
    [self setChildViewState];
    [self.controlView setTitle:_playerModel.name];
    
    [self _playWithModel:_playerModel];
}

- (void)setChildViewState {
    [self.controlView setOrientationPortraitConstraint];
    [self setCoverImage];
    self.isShiftPlayback  = NO;
    self.state         = StateStopped;
    [self _removeOldPlayer];
    self.centerPlayBtn.hidden = YES;
    if (_playerModel.action == PLAY_ACTION_AUTO_PLAY) {
        self.state         = StateBuffering;
        [self.spinner startAnimating];
    }
    
    self.controlView.disablePip = self.disablePip;
    [self.controlView setDisableOfflineBtn:!self.playerModel.isEnableCache];
    [self.controlView setTitle:self.playerModel.name];
    [self.controlView setCurrentVolume:[AVAudioSession sharedInstance].outputVolume];
    self.repeatBtn.hidden     = YES;
    // 播放时添加监听
//    [self addNotifications];
}

- (void)setCoverImage {
    if (!self.coverImageView.hidden) {
        return;
    }
    self.coverImageView.hidden = NO;
    // 建议外部赋值url
//    NSURL *customUrl = [NSURL URLWithString:_playerModel.customCoverImageUrl];
//    NSURL *defaultUrl = [NSURL URLWithString:_playerModel.defaultCoverImageUrl];
//    [self.coverImageView sd_setImageWithURL:_playerModel.customCoverImageUrl.length > 0 ? customUrl : defaultUrl
//                           placeholderImage:DuomPlayerImage(@"defaultCoverImage")
//                                    options:SDWebImageAvoidDecodeImage];
}

- (void)setVodPlayConfig {
    TXVodPlayConfig *config    = [[TXVodPlayConfig alloc] init];
    config.smoothSwitchBitrate = YES;
    config.progressInterval = 0.02;
    config.headers = self.playerConfig.headers;

    config.preferredResolution = 720 * 1280;
    [self.vodPlayer setConfig:config];
    

    self.vodPlayer.enableHWAcceleration = self.playerConfig.hwAcceleration;
    [self.vodPlayer setStartTime:self.startTime];
    self.startTime = 0;
    
    [self.vodPlayer setRate:self.playerConfig.playRate];
    [self.vodPlayer setMute:self.playerConfig.mute];
    [self.vodPlayer setRenderMode:self.playerConfig.renderMode];
    [self.vodPlayer setLoop:self.loop];
    
    [self.netWatcher startWatch];
    
    __weak DuomPlayerView *weakSelf = self;
    [self.netWatcher setNotifyTipsBlock:^(NSString *msg) {
        DuomPlayerView *strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf showMiddleBtnMsg:msg withAction:ActionSwitch];
            [strongSelf.middleBlackBtn fadeOut:2];
        }
    }];
}

- (void)controllViewPlayClick {
    [self.spinner startAnimating];
    if (!self.vodPlayer.isAutoPlay) {
        [self resume];
    } else {
        if (self.state == StateStopped) {
            [self.controlView setPlayState:YES];
            self.isPauseByUser = NO;
            self.centerPlayBtn.hidden = YES;
            [self.controlView setSliderState:YES];
            [self.controlView setTopViewState:YES];
            if (_playerModel.action == PLAY_ACTION_MANUAL_PLAY) {
                [self showOrHideBackBtn:YES];
            }
            [self startPlayWithUrl:_currentVideoUrl];
        } else {
            [self resume];
        }
    }
}

- (void)startPlayWithUrl:(NSString *)videoUrl {
    [self.vodPlayer startPlay:videoUrl];
}


/**
 *  设置Player相关参数
 */
- (void)configTXPlayer {
    LOG_ME;

    if (_playerConfig.enableLog) {
//        [TXLiveBase setLogLevel:LOGLEVEL_DEBUG];
//        [TXLiveBase sharedInstance].delegate = self;
//        [TXLiveBase setConsoleEnabled:YES];
    }

    [TXLiveBase setLogLevel:LOGLEVEL_ERROR]; // log输出限制
    [self.vodPlayer stopPlay];
    [self.livePlayer stopPlay];
    [self.vodPlayer removeVideoWidget];
    [self.livePlayer removeVideoWidget];

    [self.vodPlayer exitPictureInPicture];
    _hasStartPip = NO;
    _vodPlayer = nil;

    self.liveProgressTime = self.maxLiveProgressTime = 0;

    self.isLoaded = NO;

    self.netWatcher.playerModel = self.playerModel;
    NSString *videoURL = self.playerModel.playingDefinitionUrl;
    
    [self setVodPlayConfig];
    [self preparePlayWithVideoUrl:(videoURL)];
    
    [self resetControlViewWithLive:self.isLive shiftPlayback:self.isShiftPlayback isPlaying:self.state == StatePlaying ? YES : NO];
    self.controlView.playerConfig = self.playerConfig;
    self.repeatBtn.hidden         = YES;
    self.playDidEnd               = NO;
    [self.middleBlackBtn fadeOut:0.1];
}

- (void)preparePlayWithVideoUrl:(NSString *)videoUrl {
    _currentVideoUrl = videoUrl;
    [self.controlView setProgressTime:0 totalTime:_playerModel.duration progressValue:0 playableValue:0 / _playerModel.duration];
    if (_playerModel.action == PLAY_ACTION_AUTO_PLAY) {
        if (!DuomPlayerWindowShared.isShowing) {
            [self.controlView setPlayState:YES];
            [self.controlView setSliderState:YES];
        } else {
            [self.controlView fadeOut:0.2];
        }
        self.isPauseByUser = NO;
        self.centerPlayBtn.hidden = YES;
        [self startPlayWithUrl:videoUrl];
    } else if (_playerModel.action == PLAY_ACTION_PRELOAD) {
        self.vodPlayer.isAutoPlay = NO;
        [self startPlayWithUrl:videoUrl];
        self.spinner.hidden = YES;
        self.centerPlayBtn.hidden = NO;
        self.coverImageView.hidden = NO;
        self.isPauseByUser = YES;
        [self.controlView setPlayState:NO];
        [self.controlView setSliderState:YES];
    } else {
        if (self.state == StatePlaying) {
            [self startPlayWithUrl:videoUrl];
        } else {
            self.spinner.hidden = YES;
            self.coverImageView.hidden = NO;
            self.centerPlayBtn.hidden = NO;
            self.isPauseByUser = YES;
            [self.controlView setPlayState:NO];
            [self.controlView setSliderState:NO];
            [self.controlView setTopViewState:NO];
            [self showOrHideBackBtn:NO];
        }
    }
}


- (void)setFatherView:(UIView *)fatherView {
    if (fatherView != _fatherView) {
        [self addPlayerToFatherView:fatherView];
    }
    _fatherView = fatherView;
}


- (void)restart {
    [self.spinner startAnimating];
    self.centerPlayBtn.hidden = YES;
    self.repeatBtn.hidden = YES;
    self.playDidEnd = NO;
    
    NSString *url = self.playerModel.playingDefinitionUrl;
    if ([self.vodPlayer supportedBitrates].count > 1) {
        [self.vodPlayer resume];
    } else {
        [self.vodPlayer startPlay:url];
        if (_playerModel.action == PLAY_ACTION_PRELOAD) {
            [self resume];
        }
    }
}


- (void)setPipLoadingWithText:(NSString *)text {
    CGFloat width=[(NSString *)text boundingRectWithSize:CGSizeMake(CGFLOAT_MAX, 21)
                                                 options:NSStringDrawingUsesLineFragmentOrigin
                                              attributes:@{NSFontAttributeName:
                                                               [UIFont systemFontOfSize:DEFAULT_PIP_LOADING_FONT_SIZE]}
                                                 context:nil].size.width;
    CGFloat x = (DScreenWidth - (width + DEFAULT_PIP_LOADING_WIDTH_MARGIN)) / 2;
    CGFloat y = (DScreenHeight/2) - DEFAULT_PIP_LOADING_HEIGHT;
    CGFloat w = width + DEFAULT_PIP_LOADING_WIDTH_MARGIN;
    CGFloat h = DEFAULT_PIP_LOADING_HEIGHT * 2;
    _pipLoadingView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(x,y,w,h)];
    _pipLoadingView.backgroundColor = [UIColor blackColor];
    _pipLoadingView.layer.cornerRadius = 10;
    _pipLoadingView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(
                                                               DEFAULT_PIP_LOADING_LABEL_MARGIN,
                                                               DEFAULT_PIP_LOADING_LABEL_MARGIN + DEFAULT_PIP_LOADING_HEIGHT,
                                                               width,
                                                               DEFAULT_PIP_LOADING_LABEL_HEIGHT)];
    label.text = text;
    label.font = [UIFont systemFontOfSize:DEFAULT_PIP_LOADING_FONT_SIZE];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor whiteColor];
    
    [_pipLoadingView addSubview:label];
    [self.fatherView.viewController.view addSubview:_pipLoadingView];
}

- (void)prepareAutoplay {
    if (!self.autoPlay) {
        self.autoPlay = YES; // 下次用户设置自动播放失效
        [self pause];
    }
    
    if ([self.delegate respondsToSelector:@selector(duomPlayerDidStart:)]) {
        [self.delegate duomPlayerDidStart:self];
    }
}

- (void)detailPrepareState {
    // 防止暂停导致加载进度不消失
    if (self.isPauseByUser) [self.spinner stopAnimating];
    self.state = StatePrepare;
    if (self->_isPrepare) {
        [self.vodPlayer resume];
        self->_isPrepare = NO;
        self.isPauseByUser = NO;
        [self.controlView setPlayState:YES];
        self.centerPlayBtn.hidden = YES;
    }
}

- (void)detailProgress {
    // StateStopped 是当前播放的状态  playDidEnd 是否播放完了
    // StatePrepare 是在接受到onPlayEvent回调的状态  _isPrepare是用户主动触发resume的状态
    if (self.state == StateStopped) return;
    if (self.playDidEnd) return;
    if (_playerModel.action == PLAY_ACTION_PRELOAD) {
        // 预加载状态下才会有此判断
        if (self.state == StatePrepare || !self->_isPrepare) {
            return;
        };
    }
    
    self.state = StatePlaying;
    if (self.state == StatePlaying) {
        self.centerPlayBtn.hidden = YES;
        self.repeatBtn.hidden = YES;
        self.playDidEnd = NO;
    }
}

- (void)detailPlayerEvent:(TXVodPlayer *)player event:(int)evtID param:(NSDictionary *)param{
    if (evtID == PLAY_ERR_NET_DISCONNECT) {
        [self showMiddleBtnMsg:kStrBadNetRetry withAction:ActionContinueReplay];
    } else {
        [self showMiddleBtnMsg:kStrLoadFaildRetry withAction:ActionRetry];
    }
    self.state = StateFailed;
    [player stopPlay];
    if ([self.delegate respondsToSelector:@selector(duomPlayerError:errCode:errMessage:)]) {
        [self.delegate duomPlayerError:self errCode:evtID errMessage:param[EVT_MSG]];
    }
}

#pragma mark - Control View Configuration
- (void)resetControlViewWithLive:(BOOL)isLive shiftPlayback:(BOOL)isShiftPlayback isPlaying:(BOOL)isPlaying {
    [_controlView resetWithResolutionNames:self.playerModel.playDefinitions
                    currentResolutionIndex:self.playerModel.playingDefinitionIndex
                                    isLive:isLive
                            isTimeShifting:isShiftPlayback
                                 isPlaying:isPlaying];
}

#pragma mark - layoutSubviews
- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.subviews.count > 0) {
        UIView *innerView = self.subviews[0];
        if ([innerView isKindOfClass:NSClassFromString(@"TXIJKSDLGLView")] || [innerView isKindOfClass:NSClassFromString(@"TXCAVPlayerView")] || [innerView isKindOfClass:NSClassFromString(@"TXCThumbPlayerView")]) {
            innerView.frame = self.bounds;
        }
    }
}


#pragma mark - Action

- (void)fastViewImageAvaliable:(UIImage *)image progress:(CGFloat)draggedValue {
    if (self.controlView.isShowSecondView) return;
    [self.fastView showImg:image withProgress:draggedValue];
    [self.fastView fadeShow];
}


- (void)fastViewProgressAvaliable:(NSInteger)draggedTime {
    NSInteger totalTime = 0;
    if (_playerModel.duration > 0) {
        totalTime = _playerModel.duration;
    } else {
        totalTime = [self playDuration];
    }
    NSString *currentTimeStr = [StrUtils timeFormat:draggedTime];
    NSString *totalTimeStr   = [StrUtils timeFormat:totalTime];
    NSString *timeStr        = [NSString stringWithFormat:@"%@ / %@", currentTimeStr, totalTimeStr];
    if (self.isLive) {
        timeStr = [NSString stringWithFormat:@"%@", currentTimeStr];
    }
    
    CGFloat sliderValue = 1;
    if (totalTime > 0) {
        sliderValue = (CGFloat)draggedTime / totalTime;
    }
    if (self.isLive && totalTime > MAX_SHIFT_TIME) {
        CGFloat base = totalTime - MAX_SHIFT_TIME;
        if (self.sumTime < base) self.sumTime = base;
        sliderValue = (self.sumTime - base) / MAX_SHIFT_TIME;
        NSLog(@"%f", sliderValue);
    }
    [self.fastView showText:timeStr withText:sliderValue];
    
    [self.fastView fadeShow];
}

/**
 *  播放完了
 *
 */
- (void)moviePlayDidEnd {
    self.state      = StateStopped;
    self.playDidEnd = YES;
    // 播放结束隐藏
    if (DuomPlayerWindowShared.isShowing) {
        if (!_isVideoList) {   // 非轮播
            [DuomPlayerWindowShared hide];
            [self resetPlayer];
            DuomPlayerWindowShared.backController = nil;
        } else {  // 轮播，如果不是循环播放并且是列表的最后一个，则需要隐藏小窗口
            if (!_isLoopPlayList && _playingIndex == _videoModelList.count - 1) {
                [DuomPlayerWindowShared hide];
                [self resetPlayer];
            }
        }
    }
    [self.controlView setPlayState:NO];
    [self.controlView fadeOut:0.2];
    [self fastViewUnavaliable];
    [self.netWatcher stopWatch];
    self.repeatBtn.hidden     = NO;
    self.centerPlayBtn.hidden = YES;
    if ([self.delegate respondsToSelector:@selector(duomPlayerDidEnd:)]) {
        [self.delegate duomPlayerDidEnd:self];
    }
    
    if (_isVideoList) {
        _playingIndex = (_playingIndex < _videoModelList.count - 1) ? _playingIndex += 1 : 0;
        if (_playingIndex == 0) {
            _isLoopPlayList ? [self playModelInList:_playingIndex] : nil;
        } else {
            [self playModelInList:_playingIndex];
        }
    }
}

- (void)centerPlayBtnClick {
    self.centerPlayBtn.hidden = YES;
    [self.controlView setSliderState:YES];
    [self.controlView setTopViewState:YES];
    [self.spinner startAnimating];
    if (_playerModel.action == PLAY_ACTION_MANUAL_PLAY) {
        [self showOrHideBackBtn:YES];
    }
    
    [self controllViewPlayClick];
}

#pragma mark - DeviceOrientation
/**
 *  player添加到fatherView上
 */
- (void)addPlayerToFatherView:(UIView *)view {
    [self removeFromSuperview];
    if (view) {
        [view addSubview:self];
        [self mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_offset(UIEdgeInsetsZero);
        }];
    }
}

/**
 *  添加观察者、通知
 */
- (void)addDeviceOrientationNotifications {
        // 监测设备方向
        [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil];
    
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onStatusBarOrientationChange) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    
}

// 状态条变化通知（在前台播放才去处理）
- (void)onStatusBarOrientationChange {
    [self onDeviceOrientationChange];
    return;
//    if (!self.didEnterBackground) {
//        UIInterfaceOrientation orientation = (UIInterfaceOrientation)[UIDevice currentDevice].orientation;
//        SuperPlayerLayoutStyle style       = [self defaultStyleForDeviceOrientation:orientation];
//        //        [[UIApplication sharedApplication] setStatusBarOrientation:orientation animated:NO];
//        if ([UIApplication sharedApplication].statusBarOrientation != orientation) {
//            [self _adjustTransform:(UIInterfaceOrientation)[UIDevice currentDevice].orientation];
//        }
//        [self _switchToFullScreen:style == SuperPlayerLayoutStyleFullScreen];
//        [self _switchToLayoutStyle:style];
//    }
}

/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange {
    if (!self.isLoaded) {
        return;
    }
    if (self.isLockScreen) {
        return;
    }
    if (self.didEnterBackground) {
        return;
    };
    if (DuomPlayerWindowShared.isShowing) {
        return;
    }
    if (self.state == StatePlaying) {
        UIDeviceOrientation orientation = [UIDevice currentDevice].orientation;
        if (orientation == UIDeviceOrientationFaceUp || orientation == UIDeviceOrientationFaceDown) {
            return;
        }
        DuomPlayerLayoutStyle style = [self defaultStyleForDeviceOrientation:[UIDevice currentDevice].orientation];

        BOOL shouldFullScreen = UIDeviceOrientationIsLandscape(orientation);
        [self _switchToFullScreen:shouldFullScreen];
        [self _adjustTransform:[self _orientationForFullScreen:shouldFullScreen]];
        [self _switchToLayoutStyle:style];
    }
}

#pragma mark 屏幕转屏相关
/**
 *  屏幕转屏
 *
 *  @param orientation 屏幕方向
 */
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation {
    if (orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        // 设置横屏
        [self setOrientationLandscapeConstraint:orientation];
    } else if (orientation == UIInterfaceOrientationPortrait) {
        // 设置竖屏
        [self setOrientationPortraitConstraint];
    }
}

- (DuomPlayerLayoutStyle)defaultStyleForDeviceOrientation:(UIDeviceOrientation)orientation {
    if (UIDeviceOrientationIsPortrait(orientation)) {
        return DuomPlayerLayoutStyleCompact;
    } else {
        return DuomPlayerLayoutStyleFullScreen;
    }
}

#pragma mark - KVO

/**
 *  设置横屏的约束
 */
- (void)setOrientationLandscapeConstraint:(UIInterfaceOrientation)orientation {
    self.isFullScreen = YES;
}

/**
 *  设置竖屏的约束
 */
- (void)setOrientationPortraitConstraint {
    [self addPlayerToFatherView:self.fatherView];
    _isFullScreen = NO;
    //    [self _switchToLayoutStyle:UIInterfaceOrientationPortrait];
}

- (void)_adjustTransform:(UIDeviceOrientation)orientation {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];

    self.transform                 = [self getTransformRotationAngleOfOrientation:orientation];
    self.fullScreenBlackView.transform = self.transform;
    [UIView commitAnimations];
}

/**
 * 获取变换的旋转角度
 *
 * @return 变换矩阵
 */
- (CGAffineTransform)getTransformRotationAngleOfOrientation:(UIDeviceOrientation)orientation {
    // 状态条的方向已经设置过,所以这个就是你想要旋转的方向
    UIInterfaceOrientation interfaceOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (interfaceOrientation == (UIInterfaceOrientation)orientation) {
        return CGAffineTransformIdentity;
    }
    // 根据要进行旋转的方向来计算旋转的角度
    if (orientation == UIInterfaceOrientationPortrait) {
        return CGAffineTransformIdentity;
    } else if (orientation == UIInterfaceOrientationLandscapeLeft) {
        return CGAffineTransformMakeRotation(-M_PI_2);
    } else if (orientation == UIInterfaceOrientationLandscapeRight) {
        return CGAffineTransformMakeRotation(M_PI_2);
    }
    return CGAffineTransformIdentity;
}

- (UIDeviceOrientation)_orientationForFullScreen:(BOOL)fullScreen {
    UIDeviceOrientation targetOrientation = [UIDevice currentDevice].orientation;
    if (fullScreen) {
        if (!UIDeviceOrientationIsLandscape(targetOrientation)) {
            targetOrientation = UIDeviceOrientationLandscapeLeft;
        }
    } else {
        if (!UIDeviceOrientationIsPortrait(targetOrientation)) {
            targetOrientation = UIDeviceOrientationPortrait;
        }
    }
    return targetOrientation;
}

- (void)_switchToFullScreen:(BOOL)fullScreen {
    if (self.isFullScreen == fullScreen) {
        return;
    }
    _isFullScreen = fullScreen;
    [self.fatherView.viewController setNeedsStatusBarAppearanceUpdate];

//    UIDeviceOrientation targetOrientation = [self _orientationForFullScreen:fullScreen];  // [UIDevice currentDevice].orientation;

    if (fullScreen) {
        [self removeFromSuperview];
        [[UIApplication sharedApplication].keyWindow addSubview:self.fullScreenBlackView];
        [self.fullScreenBlackView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(DScreenHeight));
            make.height.equalTo(@(DScreenWidth));
            make.center.equalTo([UIApplication sharedApplication].keyWindow);
        }];

        [[UIApplication sharedApplication].keyWindow addSubview:self];
        [self mas_remakeConstraints:^(MASConstraintMaker *make) {
            if (IsIPhoneX) {
                make.width.equalTo(@(DScreenHeight - self.mm_safeAreaTopGap * 2));
            } else {
                make.width.equalTo(@(DScreenHeight));
            }
            make.height.equalTo(@(DScreenWidth));
            make.center.equalTo([UIApplication sharedApplication].keyWindow);
        }];
        [self.superview setNeedsLayout];
    } else {
        [self.fullScreenBlackView removeFromSuperview];
        [self addPlayerToFatherView:self.fatherView];
    }
}

- (void)_switchToLayoutStyle:(DuomPlayerLayoutStyle)style {
    // 获取到当前状态条的方向
    // 根据要旋转的方向,使用Masonry重新修改限制
    if (style == DuomPlayerLayoutStyleFullScreen) {  //
        // 这个地方加判断是为了从全屏的一侧,直接到全屏的另一侧不用修改限制,否则会出错;
        if (self.layoutStyle != DuomPlayerLayoutStyleFullScreen) {
            [self removeFromSuperview];
            [[UIApplication sharedApplication].keyWindow addSubview:self.fullScreenBlackView];
            [self.fullScreenBlackView mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.width.equalTo(@(DScreenHeight));
                make.height.equalTo(@(DScreenWidth));
                make.center.equalTo([UIApplication sharedApplication].keyWindow);
            }];

            [[UIApplication sharedApplication].keyWindow addSubview:self];
            [self mas_remakeConstraints:^(MASConstraintMaker *make) {
                if (IsIPhoneX) {
                    make.width.equalTo(@(DScreenHeight - self.mm_safeAreaTopGap * 2));
                } else {
                    make.width.equalTo(@(DScreenHeight));
                }
                make.height.equalTo(@(DScreenWidth));
                make.center.equalTo([UIApplication sharedApplication].keyWindow);
            }];
        }
    } else {
        [self.fullScreenBlackView removeFromSuperview];
    }
    self.controlView.compact = style == DuomPlayerLayoutStyleCompact;

    [[UIApplication sharedApplication].keyWindow layoutIfNeeded];
}

#pragma mark - DuomPlayerControlViewDelegate
- (void)controlViewPlay:(UIView *)controlView {
    if (self.playDidEnd) {
        [self.vodPlayer stopPlay];
        [self setVodPlayConfig];
        [self restart];
    } else {
        [self controllViewPlayClick];
    }
}

- (void)controlViewPause:(DuomPlayerControlView *)controlView {
    [self pause];
    if (self.state == StatePlaying) {
        self.state = StatePause;
    }
}

- (void)controlViewNextClick:(UIView *)controlView {
    [self.vodPlayer pause];
    [self moviePlayDidEnd];
}

- (void)controlViewBack:(DuomPlayerControlView *)controlView {
    [self controlViewBackAction:controlView];
}

- (void)controlViewBackAction:(id)sender {
    if (self.isFullScreen) {
        self.isFullScreen = NO;
        return;
    }
    if ([self.delegate respondsToSelector:@selector(duomPlayerBackAction:)]) {
        [self.delegate duomPlayerBackAction:self];
    }
}

- (void)controlViewChangeScreen:(DuomPlayerControlView *)controlView withFullScreen:(BOOL)isFullScreen {
    self.isFullScreen = isFullScreen;
    if (_playerModel.action == PLAY_ACTION_PRELOAD && (self.state == StatePause || self.state == StatePrepare)) {
        self->_isPrepare = NO;
        self.isPauseByUser = NO;
        [self.controlView setPlayState:YES];
        self.centerPlayBtn.hidden = YES;
        [self.vodPlayer resume];
        if (self.delegate && [self.delegate respondsToSelector:@selector(duomPlayerDidStart:)]) {
            [self.delegate duomPlayerDidStart:self];
        }
    }
    
    if (isFullScreen) {
        [self.controlView setTopViewState:YES];
    }
    [self showOrHideBackBtn:isFullScreen];
}

- (void)controlViewDidChangeScreen:(UIView *)controlView {
    if (self.state == StatePlaying || self.repeatBtn.hidden == NO) {
        if ([self.delegate respondsToSelector:@selector(duomPlayerFullScreenChanged:)]) {
            [self showOrHideBackBtn:self.isFullScreen];
            [self.delegate duomPlayerFullScreenChanged:self];
        }
    }
}

- (void)controlViewLockScreen:(DuomPlayerControlView *)controlView withLock:(BOOL)isLock {
    self.isLockScreen = isLock;
}

- (void)controlViewSwitch:(DuomPlayerControlView *)controlView withDefinition:(NSString *)definition {
    if ([self.playerModel.playingDefinition isEqualToString:definition]) return;
    self.playerModel.playingDefinition = definition;
    NSString *url                      = self.playerModel.playingDefinitionUrl;
    
    if (self.isLive) {
        [self.livePlayer switchStream:url];
        [self showMiddleBtnMsg:[NSString stringWithFormat:@"正在切换到%@...", definition] withAction:ActionNone];
    } else {
        self.controlView.hidden = YES;
        if (!self.playDidEnd) {
            if ([self.vodPlayer supportedBitrates].count > 1) {
                [self.vodPlayer setBitrateIndex:self.playerModel.playingDefinitionIndex];
            } else {
                CGFloat startTime = [self.vodPlayer currentPlaybackTime];
                [self.vodPlayer stopPlay];
                self.state = StateStopped;
                [self.vodPlayer setStartTime:startTime];
                [self.vodPlayer startPlay:url];
                if (_playerModel.action == PLAY_ACTION_PRELOAD) {
                    [self resume];
                }
            }
        }
    }
    
    [self.controlView setResolutionViewState:NO];
    
    [self.vodPlayer setRate:self.playerConfig.playRate];
    //        [self.vodPlayer setMirror:self.playerConfig.mirror];
    [self.vodPlayer setMute:self.playerConfig.mute];
    [self.vodPlayer setRenderMode:self.playerConfig.renderMode];
}

- (void)controlViewConfigUpdate:(DuomPlayerView *)controlView withReload:(BOOL)reload {
    if (self.state == StateStopped && !self.isLive) {
        return;
    }
    
    if (self.isLive) {
        [self.livePlayer setMute:self.playerConfig.mute];
        [self.livePlayer setRenderMode:self.playerConfig.renderMode];
    } else {
        [self.vodPlayer setRate:self.playerConfig.playRate];
//        [self.vodPlayer setMirror:self.playerConfig.mirror];
        [self.vodPlayer setMute:self.playerConfig.mute];
        [self.vodPlayer setRenderMode:self.playerConfig.renderMode];
    }
    if (reload) {
        if (!self.isLive) self.startTime = [self.vodPlayer currentPlaybackTime];
        self.isShiftPlayback = NO;
        [self configTXPlayer];
        if (_playerModel.action == PLAY_ACTION_PRELOAD) {
            self.state = StateStopped;
            [self resume];
        }
        
        if (self.state != StatePlaying && _playerModel.action == PLAY_ACTION_MANUAL_PLAY) {
            [self controllViewPlayClick];
        }
    }
}

- (void)controlViewReload:(UIView *)controlView {
    if (self.isLive) {
        self.isShiftPlayback = NO;
        self.isLoaded        = NO;
        [self.livePlayer resumeLive];
        [self resetControlViewWithLive:self.isLive shiftPlayback:self.isShiftPlayback isPlaying:YES];
    } else {
        self.startTime = [self.vodPlayer currentPlaybackTime];
        [self configTXPlayer];
    }
}

- (CGFloat)sliderPosToTime:(CGFloat)pos {
    // 视频总时间长度
    CGFloat totalTime = 0;
    if (_playerModel.duration > 0) {
        totalTime = _playerModel.duration;
    } else {
        totalTime = [self playDuration];
    }

    //计算出拖动的当前秒数
    CGFloat dragedSeconds = floorf(totalTime * pos);
    if (self.isLive && totalTime > MAX_SHIFT_TIME) {
        CGFloat base  = totalTime - MAX_SHIFT_TIME;
        dragedSeconds = floor(MAX_SHIFT_TIME * pos) + base;
    }
    return dragedSeconds;
}

- (void)controlViewSeek:(DuomPlayerControlView *)controlView where:(CGFloat)pos {
    CGFloat dragedSeconds = [self sliderPosToTime:pos];
    if (_playerModel.action == PLAY_ACTION_PRELOAD) {
        self->_isPrepare = NO;
        self.isPauseByUser = NO;
        [self.controlView setPlayState:YES];
        self.centerPlayBtn.hidden = YES;
        [self.vodPlayer resume];
        [self.vodPlayer seek:dragedSeconds];
        if (self.delegate && [self.delegate respondsToSelector:@selector(duomPlayerDidStart:)]) {
            [self.delegate duomPlayerDidStart:self];
        }
    } else {
        if (self.state == StateStopped) {
            [self.vodPlayer setStartTime:dragedSeconds];
            [self.vodPlayer startPlay:_currentVideoUrl];
        } else {
            [self seekToTime:dragedSeconds];
        }
        
        [self.controlView setPlayState:YES];
        self.centerPlayBtn.hidden = YES;
        self.repeatBtn.hidden = YES;

    }
    [self fastViewUnavaliable];
}

- (void)controlViewPreview:(DuomPlayerControlView *)controlView where:(CGFloat)pos {
    CGFloat dragedSeconds = [self sliderPosToTime:pos];
    if ([self playDuration] > 0) {  // 当总时长 > 0时候才能拖动slider
        [self fastViewProgressAvaliable:dragedSeconds];
    }
}

- (void)controlViewPip:(UIView *)controlView {
    if (![TXVodPlayer isSupportPictureInPicture]) {
        return;
    }
    
    if (_hasStartPip) {
        return;
    } else {
        
        if (_hasStartPipLoading) {
            return;
        }
        
        if (self.state == StateStopped) {
            return;
        }
        
        [self setPipLoadingWithText:PIP_START_LOADING_TEXT];
        [self.pipLoadingView startAnimating];
        _hasStartPipLoading = YES;
        [_vodPlayer enterPictureInPicture];
    }
}


- (void)onBackClick {
    self.centerPlayBtn.hidden = YES;
    if (!self.isFullScreen) {
        [self seekToTime:0];
    } else {
        self.isFullScreen = NO;
    }
}

- (void)onRepeatClick {
    self.centerPlayBtn.hidden = YES;
    [self seekToTime:0];
}

- (void)controlViewVolumeChange:(UIView *)controlView value:(CGFloat)value {
    [[self class] volumeViewSlider].value = value;
}


- (void)volumeChanged:(NSNotification *)notification {
    if (self.isDragging) return;  // 正在拖动，不响应音量事件

    if (![[[notification userInfo] objectForKey:VOLUME_CHANGE_PARAMATER] isEqualToString:VOLUME_EXPLICIT_CHANGE]) {
        return;
    }
    float volume = [[[notification userInfo] objectForKey:VOLUME_CHANGE_KEY] floatValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.disableVolumControl) {
            [self fastViewImageAvaliable:DuomPlayerImage(@"sound_max") progress:volume];
            [self.fastView fadeOut:1];
        }
    });
}


#pragma mark - middle btn
- (void)showMiddleBtnMsg:(NSString *)msg withAction:(ButtonAction)action {
    [self.middleBlackBtn setTitle:msg forState:UIControlStateNormal];
    self.middleBlackBtn.titleLabel.text = msg;
    self.middleBlackBtnAction           = action;
    CGFloat width                       = self.middleBlackBtn.titleLabel.attributedText.size.width;

    [self.middleBlackBtn mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(width + 10));
    }];
    [self.middleBlackBtn fadeShow];
}

#pragma mark - 点播回调

- (void)_removeOldPlayer {
    for (UIView *w in [self subviews]) {
        if ([w isKindOfClass:NSClassFromString(@"TXCRenderView")]) [w removeFromSuperview];
        if ([w isKindOfClass:NSClassFromString(@"TXIJKSDLGLView")]) [w removeFromSuperview];
        if ([w isKindOfClass:NSClassFromString(@"TXCAVPlayerView")]) [w removeFromSuperview];
        if ([w isKindOfClass:NSClassFromString(@"TXCThumbPlayerView")]) [w removeFromSuperview];
    }
}

- (void)onPlayEvent:(TXVodPlayer *)player event:(int)EvtID withParam:(NSDictionary *)param {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (EvtID != PLAY_EVT_PLAY_PROGRESS) {
            NSString *desc = [param description];
            NSLog(@"%@", [NSString stringWithCString:[desc cStringUsingEncoding:NSUTF8StringEncoding] encoding:NSNonLossyASCIIStringEncoding]);
        }

        float duration = self->_playerModel.duration > 0 ? self->_playerModel.duration : player.duration;
        
        if (EvtID == PLAY_EVT_RCV_FIRST_I_FRAME) {
            self.state = StateFirstFrame;
        }

        if (EvtID == EVT_VIDEO_PLAY_BEGIN) {
            [self setNeedsLayout];
            [self layoutIfNeeded];
            self.isLoaded = YES;
            [self _removeOldPlayer];
            [self.vodPlayer setupVideoWidget:self insertIndex:0];
            [self layoutSubviews];  // 防止横屏状态下添加view显示不全

//            for (DuomVideoFrameDescription *p in self.keyFrameDescList) {
//                if (player.duration > 0) p.where = p.time / duration;
//            }
//            self.controlView.pointArray = self.keyFrameDescList;
            self.state = StatePlaying;
            [self.controlView setPlayState:YES];
            self.centerPlayBtn.hidden = YES;
            self.repeatBtn.hidden = YES;
            self.playDidEnd = NO;
            self.controlView.hidden = DuomPlayerWindowShared.isShowing ? YES : NO;
            // 不使用vodPlayer.autoPlay的原因是暂停的时候会黑屏，影响体验
            [self prepareAutoplay];
        }
        if (EvtID == PLAY_EVT_VOD_PLAY_PREPARED) {
            [self updateBitrates:player.supportedBitrates];
            [self detailPrepareState];
        }
        if (EvtID == PLAY_EVT_PLAY_PROGRESS) {
            [self detailProgress];
            self.playCurrentTime = player.currentPlaybackTime;
            CGFloat totalTime    = duration;
            CGFloat value        = player.currentPlaybackTime / duration;
            CGFloat playable     = player.playableDuration / duration;
            
            if (self.state == StatePlaying) {
                [self.controlView setProgressTime:self.playCurrentTime totalTime:totalTime progressValue:value playableValue:playable];
//                [self.vipWatchView setCurrentTime:self.playCurrentTime];
            }
            
        } else if (EvtID == PLAY_EVT_PLAY_END) {
            [self.controlView setProgressTime:[self playDuration] totalTime:[self playDuration] progressValue:player.duration / duration playableValue:player.duration / duration];
            [self moviePlayDidEnd];
        } else if (EvtID == PLAY_ERR_NET_DISCONNECT || EvtID == PLAY_ERR_FILE_NOT_FOUND || EvtID == PLAY_ERR_HLS_KEY) {
            [self detailPlayerEvent:player event:EvtID param:param];
            
        } else if (EvtID == PLAY_EVT_PLAY_LOADING) {
            // 当缓冲是空的时候
            if (self->_playerModel.action != PLAY_ACTION_PRELOAD) {
                self.state = StateBuffering;
            }
        } else if (EvtID == PLAY_EVT_VOD_LOADING_END) {
            [self.spinner stopAnimating];
        } else if (EvtID == PLAY_EVT_CHANGE_RESOLUTION) {
            if (player.height != 0) {
                self.videoRatio = (GLfloat)player.width / player.height;
            }
        }
        
        [self onVodPlayEvent:player event:EvtID withParam:param];
    });
}

- (void)onVodPlayEvent:(TXVodPlayer *)player event:(int)evtID withParam:(NSDictionary *)param
{
    if ([self.playListener respondsToSelector:@selector(onVodPlayEvent:event:withParam:)]) {
        [self.playListener onVodPlayEvent:player event:evtID withParam:param];
    }
}

-(void)onNetStatus:(TXVodPlayer *)player withParam:(NSDictionary*)param
{
    NSDictionary *dict = param;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([self.playListener respondsToSelector:@selector(onVodNetStatus:withParam:)]) {
            [self.playListener onVodNetStatus:player withParam:dict];
        }
    });
}

// 更新当前播放的视频信息，包括清晰度、码率等
- (void)updateBitrates:(NSArray<TXBitrateItem *> *)bitrates;
{
    // 播放离线视频，不更新清晰度，离线视频只有一个清晰度
    if ([_currentVideoUrl containsString:@"/var/mobile/Containers/Data/Application/"]) {
        return;
    }
    
    if (bitrates.count > 0) {
        if (self.resolutions) {
            if (_playerModel.multiVideoURLs == nil) {
                NSMutableArray *urlDefs = [[NSMutableArray alloc] initWithCapacity:self.resolutions.count];
                for (DuomSubStreamInfo *info in self.resolutions) {
                    DuomPlayerUrl *url = [[DuomPlayerUrl alloc] init];
                    url.title           = info.resolutionName;
                    [urlDefs addObject:url];
                }
                _playerModel.playingDefinition = _playerModel.multiVideoURLs.firstObject.title;
            }
        } else {
            NSArray *titles             = [TXBitrateItemHelper sortWithBitrate:bitrates];
            _playerModel.multiVideoURLs = titles;
            self.netWatcher.playerModel = _playerModel;
            NSInteger index = self.vodPlayer.bitrateIndex;
            if (_playerModel.playDefinitions.count > 0) {
                if (_playerModel.playDefinitions.count > index) {
                    _playerModel.playingDefinition = _playerModel.playDefinitions[index < 0 ? 0 : index];
                } else {
                    _playerModel.playingDefinition = _playerModel.playDefinitions.lastObject;
                }
            }
        }
        [self resetControlViewWithLive:self.isLive shiftPlayback:self.isShiftPlayback isPlaying:self.state == StatePlaying ? YES : NO];
        if (_isFullScreen) {
            [self.controlView setOrientationLandscapeConstraint];
        }
    }
}

#pragma mark - 直播回调


#pragma mark - 画中画回调
/**
 * 画中画状态回调
 */
- (void)onPlayer:(TXVodPlayer *)player pictureInPictureStateDidChange:(TX_VOD_PLAYER_PIP_STATE)pipState withParam:(NSDictionary *)param {
    if (pipState == TX_VOD_PLAYER_PIP_STATE_DID_START) {
        _hasStartPip = YES;
        dispatch_async(dispatch_get_main_queue(), ^{
            // 需要关掉菊花
            [self.pipLoadingView stopAnimating];
            self->_hasStartPipLoading = NO;
        });
        
    }
    
    if (pipState == TX_VOD_PLAYER_PIP_STATE_RESTORE_UI) {
        _restoreUI = YES;
        [player exitPictureInPicture];
    }
    
    if (pipState == TX_VOD_PLAYER_PIP_STATE_DID_STOP) {
        _hasStartPip = NO;
        if (!_playDidEnd && _restoreUI) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [player resume];
                self->_restoreUI = NO;
            });
        } else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self pause];
                [player stopPlay];
                [player removeVideoWidget];
                [self.vodPlayer stopPlay];
                [self.vodPlayer removeVideoWidget];
                self.vodPlayer = nil;
                self.state = StateStopped;
            });
        }
    }
}

/**
 * 画中画状态回调
 */
- (void)onPlayer:(TXVodPlayer *)player pictureInPictureErrorDidOccur:(TX_VOD_PLAYER_PIP_ERROR_TYPE)errorType withParam:(NSDictionary *)param {
    if (errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_DEVICE_NOT_SUPPORT
        || errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_PLAYER_NOT_SUPPORT
        || errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_VIDEO_NOT_SUPPORT
        || errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_PIP_IS_NOT_POSSIBLE
        || errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_ERROR_FROM_SYSTEM
        || errorType == TX_VOD_PLAYER_PIP_ERROR_TYPE_PIP_NOT_RUNNING) {
        _hasStartPip = NO;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (self->_hasStartPipLoading) {
                return;
            }
            
            [self setPipLoadingWithText:PIP_ERROR_LOADING_TEXT];
            [self.pipLoadingView startAnimating];
            self->_hasStartPipLoading = YES;
            
            dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, (ino64_t)(0.5 * NSEC_PER_SEC));
            dispatch_after(time, dispatch_get_main_queue(), ^{
                [self.pipLoadingView stopAnimating];
                self->_hasStartPipLoading = NO;
            });
        });
    }
}
#pragma mark -- setter
/**
 *  设置播放的状态
 *
 *  @param state SuperPlayerState
 */
- (void)setState:(DuomPlayerState)state {
    _state = state;
    // 控制菊花显示、隐藏
    if (state == StateBuffering) {
        [self.spinner startAnimating];
    } else {
        [self.spinner stopAnimating];
    }
    if (state == StatePlaying) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:VOLUME_NOTIFICATION_NAME object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:VOLUME_NOTIFICATION_NAME object:nil];
    } else if (state == StateFirstFrame) {
        _state = StatePlaying;
        if (self.coverImageView.alpha == 1) {
            [UIView animateWithDuration:0.2
                             animations:^{
                                 self.coverImageView.hidden = YES;
                             }];
        }
    } else if (state == StateFailed) {
    } else if (state == StateStopped) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:VOLUME_NOTIFICATION_NAME object:nil];
        
        // 如果在播放结束需要显示封面，打开此行
//        self.coverImageView.hidden = NO;

    } else if (state == StatePause) {
        // 如果在播放暂停需要显示封面，打开此行
//        self.coverImageView.hidden = NO;
    }
}


- (void)setControlView:(DuomPlayerControlView *)controlView {
    if (_controlView == controlView) {
        return;
    }
    [_controlView removeFromSuperview];

    _controlView         = controlView;
    _controlView.title = self.playerModel.name;
    controlView.delegate = self;
    [self addSubview:controlView];
    [controlView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(UIEdgeInsetsZero);
    }];
    [self resetControlViewWithLive:self.isLive shiftPlayback:self.isShiftPlayback isPlaying:self.state == StatePlaying ? YES : NO];
    [controlView setTitle:_controlView.title];
}

- (void)setDragging:(BOOL)dragging {
    _isDragging = dragging;
    if (dragging) {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:VOLUME_NOTIFICATION_NAME object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:VOLUME_NOTIFICATION_NAME object:nil];
    }
}

- (void)setLoop:(BOOL)loop {
    _loop = loop;
    if (self.vodPlayer) {
        self.vodPlayer.loop = loop;
    }
}

/** 全屏 */
- (void)setFullScreen:(BOOL)fullScreen {
    if (_isFullScreen != fullScreen) {
        [self _adjustTransform:[self _orientationForFullScreen:fullScreen]];
        [self _switchToFullScreen:fullScreen];
        [self _switchToLayoutStyle:fullScreen ? DuomPlayerLayoutStyleFullScreen : DuomPlayerLayoutStyleCompact];
    }
    _isFullScreen = fullScreen;
}


#pragma mark -- getter
- (TXVodPlayer *)vodPlayer {
    if (!_vodPlayer) {
        _vodPlayer = [[TXVodPlayer alloc] init];
        _vodPlayer.vodDelegate = self;
    }
    return _vodPlayer;
}

- (DuomPlayerFastView *)fastView {
    if (_fastView == nil) {
        _fastView = [[DuomPlayerFastView alloc] init];
        [self addSubview:_fastView];
        [_fastView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
    return _fastView;
}

- (UIButton *)middleBlackBtn {
    if (!self.isLive) {
        return nil;
    }

    if (_middleBlackBtn == nil) {
        _middleBlackBtn = [UIButton buttonWithType:UIButtonTypeSystem];
        [_middleBlackBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _middleBlackBtn.titleLabel.font = [UIFont systemFontOfSize:14.0];
        _middleBlackBtn.backgroundColor = RGBA(0, 0, 0, 0.7);
//        [_middleBlackBtn addTarget:self action:@selector(middleBlackBtnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_middleBlackBtn];
        [_middleBlackBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self);
            make.height.mas_equalTo(33);
        }];
    }
    return _middleBlackBtn;
}

- (DuomPlayerControlView *)controlView {
    if (_controlView == nil) {
        self.controlView = [[DuomDefaultControlView alloc] initWithFrame:CGRectZero];
    }
    return _controlView;
}

- (UIImageView *)coverImageView {
    if (!_coverImageView) {
        _coverImageView                        = [[UIImageView alloc] init];
        _coverImageView.userInteractionEnabled = YES;
        _coverImageView.clipsToBounds = YES;
        _coverImageView.image = DuomPlayerImage(@"defaultCoverImage");
        _coverImageView.contentMode            = UIViewContentModeScaleAspectFill;
        [self insertSubview:_coverImageView belowSubview:self.controlView];
        [_coverImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.mas_equalTo(UIEdgeInsetsZero);
        }];
    }
    return _coverImageView;
}

- (CGFloat)playDuration {
    if (self.isLive) {
        return self.maxLiveProgressTime;
    }
    return self.vodPlayer.duration;
}

- (CGFloat)playCurrentTime {
    if (self.isLive) {
        if (self.isShiftPlayback) {
            return self.liveProgressTime;
        }
        return self.maxLiveProgressTime;
    }

    return _playCurrentTime;
}

+ (UISlider *)volumeViewSlider {
    return _volumeSlider;
}

@end
