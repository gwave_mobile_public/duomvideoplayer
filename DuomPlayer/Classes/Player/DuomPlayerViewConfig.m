//
//  DuomPlayerViewConfig.m
//  Pods
//
//  Created by kuroky on 2022/9/6.
//

#import "DuomPlayerViewConfig.h"
#import "DuomPlayer.h"
#import "TXLiveSDKTypeDef.h"

@implementation DuomPlayerViewConfig

-(instancetype)init {
    self                 = [super init];
    self.hwAcceleration  = 1;
    self.playRate        = 1;
    self.renderMode      = RENDER_MODE_FILL_EDGE;
    self.maxCacheItem    = 5;
    self.enableLog       = NO;
    return self;
}

- (BOOL)hwAcceleration {
#if TARGET_OS_SIMULATOR
    return NO;
#else
    return _hwAcceleration;
#endif
}

@end
