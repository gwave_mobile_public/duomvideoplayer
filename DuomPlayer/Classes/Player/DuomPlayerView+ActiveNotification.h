//
//  DuomPlayerView+Notifications.h
//  DuomPlayer
//
//  Created by kuroky on 2022/11/3.
//

#import "DuomPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DuomPlayerView (ActiveNotification)

- (void)addActiveNotifications;

- (void)fastViewUnavaliable;

@end

NS_ASSUME_NONNULL_END
