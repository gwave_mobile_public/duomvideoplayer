//
//  DuomPlayerView+Notifications.m
//  DuomPlayer
//
//  Created by kuroky on 2022/11/3.
//

#import "DuomPlayerView+ActiveNotification.h"
#import "DuomPlayerView+Private.h"
#import "DuomPlayerView.h"
#import "TXVodPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "DuomPlayer.h"
#import "DuomPlayerFastView.h"
#import "UIView+Fade.h"


// 前后台切换
@implementation DuomPlayerView (ActiveNotification)

/**
 *  添加观察者、通知
 */
- (void)addActiveNotifications {
    // app退到后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground:) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayground:) name:UIApplicationDidBecomeActiveNotification object:nil];
}


#pragma mark - Active Notifications
/**
 *  应用退到后台
 */
- (void)appDidEnterBackground:(NSNotification *)notify {
    [self fastViewUnavaliable];
    NSLog(@"appDidEnterBackground");
    self.didEnterBackground = YES;
    if (self.isLive) {
        return;
    }
    
    if (self.hasStartPip) {
        return;
    }
    
    if (!self.isPauseByUser && (self.state != StateStopped && self.state != StateFailed)) {
        [self.vodPlayer pause];
        self.state = StatePause;
    }
}

/**
 *  应用进入前台
 */
- (void)appDidEnterPlayground:(NSNotification *)notify {
    [self fastViewUnavaliable];
    NSLog(@"appDidEnterPlayground");
    self.didEnterBackground = NO;
    if (self.isLive) {
        return;
    }
    
    if (self.hasStartPip) {
        return;
    }
    
    if (!self.isPauseByUser && (self.state != StateStopped && self.state != StateFailed)) {
        self.state = StatePlaying;
        [self.vodPlayer resume];
        CGFloat value        = self.vodPlayer.currentPlaybackTime / self.vodPlayer.duration;
        CGFloat playable     = self.vodPlayer.playableDuration / self.vodPlayer.duration;
        self.controlView.isDragging = NO;
        [self.controlView setProgressTime:self.playCurrentTime totalTime:self.vodPlayer.duration progressValue:value playableValue:playable];
        [self.vodPlayer seek:self.playCurrentTime];
    } else if (self.state != StateStopped) {
        self.repeatBtn.hidden = YES;
    }
}

- (void)fastViewUnavaliable {
    [self.fastView fadeOut:0.1];
}

@end
