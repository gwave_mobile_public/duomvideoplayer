//
//  DuomPlayerView+Private.h
//  TXLiteAVDemo
//
//  Created by annidyfeng on 2018/7/9.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#ifndef DuomPlayerView_Private_h
#define DuomPlayerView_Private_h
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AFNetworkReachabilityManager.h"
#import "Masonry/Masonry.h"
#import "NetWatcher.h"
#import "DuomPlayer.h"
#import "DuomPlayerControlViewDelegate.h"
#import "DuomPlayerFastView.h"
#import "DuomSubStreamInfo.h"
#import "MMMaterialDesignSpinner.h"

// 枚举值，包含水平移动方向和垂直移动方向
typedef NS_ENUM(NSInteger, PanDirection) {
    PanDirectionHorizontalMoved,  // 横向移动
    PanDirectionVerticalMoved     // 纵向移动
};

typedef NS_ENUM(NSInteger, ButtonAction) {
    ActionNone,
    ActionRetry,
    ActionSwitch,
    ActionIgnore,
    ActionContinueReplay,
};

@class TXVodPlayer, TXLivePlayer;
@interface DuomPlayerView () <UIGestureRecognizerDelegate, UIAlertViewDelegate, DuomPlayerControlViewDelegate, AVAssetResourceLoaderDelegate>

/** 用来保存快进的总时长 */
@property(nonatomic, assign) CGFloat sumTime;
@property(nonatomic, assign) CGFloat startVeloctyPoint;

/** 定义一个实例变量，保存枚举值 */
@property(nonatomic, assign) PanDirection panDirection;
/** 是否在调节音量*/
@property(nonatomic, assign) BOOL isVolume;
/** 是否被用户暂停 */
@property(nonatomic, assign) BOOL isPauseByUser;
/** 播放完了*/
@property(nonatomic, assign) BOOL playDidEnd;
/** 进入后台*/
@property(nonatomic, assign) BOOL didEnterBackground;
/** 单击 */
@property(nonatomic, strong) UITapGestureRecognizer *singleTap;
/** 快进快退、View*/
@property(nonatomic, strong) DuomPlayerFastView *fastView;

@property(nonatomic, setter=setDragging:) BOOL isDragging;
/// 中间的提示按钮
@property(nonatomic, strong) UIButton *middleBlackBtn;
@property ButtonAction                 middleBlackBtnAction;

/** 系统菊花 */
@property(nonatomic, strong) MMMaterialDesignSpinner *spinner;

@property(nonatomic, strong) UIButton *lockTipsBtn;

@property(class, readonly) UISlider *volumeViewSlider;

@property(nonatomic, strong) MPVolumeView *volumeView;

// add for txvodplayer
@property(nonatomic, assign) BOOL isLoaded;

@property(nonatomic, assign) BOOL isShiftPlayback;

@property(nonatomic, assign) CGFloat maxLiveProgressTime;  // 直播最大进度/总时间
@property(nonatomic, assign) CGFloat liveProgressTime;     // 直播播放器回调过来的时间
@property(nonatomic, assign) CGFloat liveProgressBase;     // 直播播放器超出时移的最大时间
#define MAX_SHIFT_TIME (2 * 60 * 60)
/** 是否是直播流 */
@property(nonatomic, assign) BOOL isLive;

/** 腾讯点播播放器 */
@property(nonatomic, strong) TXVodPlayer *vodPlayer;
/** 腾讯直播播放器 */
@property(nonatomic, strong) TXLivePlayer *livePlayer;

@property(nonatomic, assign) NSDate *reportTime;

@property(nonatomic, strong) NetWatcher *netWatcher;

@property(nonatomic, assign) CGFloat videoRatio;

/// 由协议解析出分辨率定义表
@property(nonatomic, strong) NSArray<DuomSubStreamInfo *> *resolutions;
/// 当前可用的分辨率列表
//@property (strong, nonatomic) NSArray<NSString *> *currentResolutionNames;


/*
 由DuomPlayerView.m 迁出的私有属性，为了cateogry可以使用
 */
/** 是否开启了画中画 */
@property(nonatomic, assign) BOOL hasStartPip;

@property(nonatomic, strong) UIView *fullScreenBlackView;

@end

// ---------------------------------------------------------------

@class AdaptiveStream;

@interface DuomPlayerModel ()

//@property (nonatomic, strong) NSString *drmType;
@property NSMutableArray<AdaptiveStream *> *streams;

//- (BOOL)canSetDrmType:(NSString *)drmType;

@end

#endif /* DuomPlayerView_Private_h */
