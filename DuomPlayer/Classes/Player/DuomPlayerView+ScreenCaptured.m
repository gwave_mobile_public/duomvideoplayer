//
//  DuomPlayerView+ScreenCaptured.m
//  DuomPlayer
//
//  Created by kuroky on 2022/11/7.
//

#import "DuomPlayerView+ScreenCaptured.h"
#import <AVFoundation/AVFoundation.h>
#import "TXVodPlayer.h"
#import "DuomPlayerView.h"
#import "DuomPlayerView+Private.h"
#import <objc/runtime.h>

@implementation DuomPlayerView (ScreenCaptured)

static void *captureAlertKey = &captureAlertKey;
static void *captureWarnKey = &captureWarnKey;

- (void)setAlert:(UIAlertController *)alert {
    objc_setAssociatedObject(self, &captureAlertKey, alert, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIAlertController *)alert {
    return objc_getAssociatedObject(self, &captureAlertKey);
}

- (void)addScreenCapturedNotification {
    [self screenCaptureInitState];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(screenCaptureAlert) name:UIScreenCapturedDidChangeNotification object:nil];
}

// 初始进入页面 录屏状态
- (void)screenCaptureInitState {
    UIScreen *sc = [UIScreen mainScreen];
    if (!sc.isCaptured) {
        return;
    }
    [self hiddenPlayerViewFromParent:NO];
}

// 录屏状态监听事件
- (void)screenCaptureAlert {
    UIScreen *sc = [UIScreen mainScreen];
    if (sc.isCaptured) {
        [self hiddenPlayerViewFromParent:YES];
        self.alert = [UIAlertController alertControllerWithTitle:@"warning" message:@"can not capture /n please cloase it" preferredStyle:UIAlertControllerStyleAlert];
        [[self currentViewController] presentViewController:self.alert animated:YES completion:nil];
    } else {
        [self.alert dismissViewControllerAnimated:YES completion:nil];
        [self hiddenPlayerViewFromParent:NO];
    }
}

- (void)hiddenPlayerViewFromParent: (BOOL)hidden {
    self.hidden = hidden;
    if (hidden) {
        [self.vodPlayer pause];
    } else {
        [self.vodPlayer resume];
    }
}

- (UIViewController *)currentViewController {
    for(UIView *next = [self superview]; next; next = next.superview) {
        UIResponder *nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return(UIViewController*)nextResponder;
        }
    }
    return nil;
}

@end
