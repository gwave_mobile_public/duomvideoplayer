//
//  DuomPlayerWindow.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/7.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DuomPlayerView;

typedef void (^DuomPlayerWindowEventHandler)(void);

/// 播放器小窗Window
@interface DuomPlayerWindow : UIWindow

/// 显示小窗
- (void)show;
/// 隐藏小窗
- (void)hide;
/// 单例
+ (instancetype)sharedInstance;

@property(nonatomic, copy) DuomPlayerWindowEventHandler backHandler;
@property(nonatomic, copy) DuomPlayerWindowEventHandler closeHandler;  // 默认关闭
/// 小窗播放器
@property(nonatomic, weak) DuomPlayerView *superPlayer;
/// 小窗主view
@property(readonly) UIView *rootView;
/// 点击小窗返回的controller
@property UIViewController *backController;
/// 小窗是否显示
@property(readonly) BOOL isShowing;  //


@end

