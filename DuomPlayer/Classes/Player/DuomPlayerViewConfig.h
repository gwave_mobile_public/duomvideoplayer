//
//  DuomPlayerViewConfig.h
//  Pods
//
//  Created by kuroky on 2022/9/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DuomPlayerViewConfig : NSObject
///// 是否镜像，默认NO
//@property BOOL mirror;
/// 是否硬件加速，默认YES
@property(nonatomic, assign) BOOL hwAcceleration;
/// 播放速度，默认1.0
@property(nonatomic, assign) CGFloat playRate;
/// 是否静音，默认NO
@property(nonatomic, assign) BOOL mute;
/// 填充模式，默认铺满。 参见 TXLiveSDKTypeDef.h
@property(nonatomic, assign) NSInteger renderMode;
/// http头，跟进情况自行设置
@property(nonatomic, strong) NSDictionary *headers;
/// 播放器最大缓存个数
@property(nonatomic, assign) NSInteger maxCacheItem;
///// 时移域名，默认为playtimeshift.live.myqcloud.com
//@property NSString *playShiftDomain;
/// log打印
@property(nonatomic, assign) BOOL enableLog;


@end

NS_ASSUME_NONNULL_END
