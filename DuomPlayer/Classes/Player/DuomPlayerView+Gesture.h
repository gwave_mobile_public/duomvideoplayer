//
//  DuomPlayerView+Gesture.h
//  DuomPlayer
//
//  Created by kuroky on 2022/11/3.
//

#import "DuomPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface DuomPlayerView (Gesture)

- (void)createGesture;

@end

NS_ASSUME_NONNULL_END
