//
//  DuomPlayerView+Gesture.m
//  DuomPlayer
//
//  Created by kuroky on 2022/11/3.
//

#import "DuomPlayerView+Gesture.h"
#import "DuomPlayerView+Private.h"
#import "DuomPlayerView.h"
#import "TXVodPlayer.h"
#import <AVFoundation/AVFoundation.h>
#import "DuomPlayer.h"
#import "DuomPlayerFastView.h"
#import "UIView+Fade.h"
#import "UIView+MMLayout.h"

@implementation DuomPlayerView (Gesture)

/**
 *  创建手势
 */
- (void)createGesture {
    // 单击
    self.singleTap                         = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapAction:)];
    self.singleTap.delegate                = self;
    self.singleTap.numberOfTouchesRequired = 1;  //手指数
    self.singleTap.numberOfTapsRequired    = 1;
    [self addGestureRecognizer:self.singleTap];

    // 解决点击当前view时候响应其他控件事件
    [self.singleTap setDelaysTouchesBegan:YES];
}

/**
 *   轻拍方法
 *
 *  @param gesture UITapGestureRecognizer
 */
- (void)singleTapAction:(UIGestureRecognizer *)gesture {
    if (gesture.state == UIGestureRecognizerStateRecognized) {
        if (DuomPlayerWindowShared.isShowing) return;
        
        CGPoint point = [gesture locationInView:self];
        CGFloat bottom = UIScreen.mainScreen.bounds.size.width - (BOTTOM_IMAGE_VIEW_HEIGHT + self.mm_safeAreaLeftGap);
        CGFloat top = BOTTOM_IMAGE_VIEW_HEIGHT;
        if (self.isFullScreen && (point.y > bottom || point.y < top)) {
            return;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(singleTapClick)] && self.isFullScreen) {
            [self.delegate singleTapClick];
        }

        if (self.controlView.hidden) {
            [self.controlView fadeShow];
        } else {
            [self.controlView fadeOut:0.2];
        }
        
        if (self.isFullScreen) {
            [self.controlView setTopViewState:!self.isLockScreen];
        } else {
            [self.controlView setTopViewState:(self.state == StateStopped && self.playerModel.action == PLAY_ACTION_MANUAL_PLAY) ? NO : YES];
        }
    }
}

#pragma mark - UIPanGestureRecognizer手势方法
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) {
        if (((UITapGestureRecognizer *)gestureRecognizer).numberOfTapsRequired == 2) {
            if (self.isLockScreen == YES) return NO;
        }
        return YES;
    }

    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        
        if (!self.isLoaded) {
            return NO;
        }
        if (self.isLockScreen) {
            return NO;
        }
        if (DuomPlayerWindowShared.isShowing) {
            return NO;
        }
        
        if (self.isFullScreen) {
            return YES;
        } else {
            UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
            CGPoint veloctyPoint = [pan velocityInView:self];
            CGFloat pointX = fabs(veloctyPoint.x);
            CGFloat pointY = fabs(veloctyPoint.y);
            if (pointX > pointY) {
                return YES;
            } else {
                return self.disableGesture ? YES : NO;
            }
        }
        
        return YES;
    }

    return NO;
}


@end
