//
//  DuomVideoFrameDescription.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/7.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DuomVideoFrameDescription : NSObject
// updates in SuperPlayerView
@property double where;
@property NSString *text;
@property double    time;
+ (instancetype)instanceFromDictionary:(NSDictionary *)dict;
@end

NS_ASSUME_NONNULL_END
