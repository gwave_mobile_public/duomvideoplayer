//
//  DuomVideoFrameDescription.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/7.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomVideoFrameDescription.h"

#import "J2Obj.h"

@implementation DuomVideoFrameDescription
+ (instancetype)instanceFromDictionary:(NSDictionary *)keyFrameDesc {
    if (![keyFrameDesc isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    DuomVideoFrameDescription *ret = [[DuomVideoFrameDescription alloc] init];
    ret.time                     = [J2Num([keyFrameDesc valueForKeyPath:@"timeOffset"]) intValue] / 1000.0;
    ret.text                     = [J2Str([keyFrameDesc valueForKeyPath:@"content"]) stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return ret;
}
@end
