//
//  DuomSubStreamInfo.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/7.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomSubStreamInfo.h"
#import "J2Obj.h"

@implementation DuomSubStreamInfo

+ (instancetype)infoWithDictionary:(NSDictionary *)dict {
    DuomSubStreamInfo *info   = [[DuomSubStreamInfo alloc] init];
    double           width  = [J2Num(dict[@"width"]) doubleValue];
    double           height = [J2Num(dict[@"height"]) doubleValue];
    info.size               = CGSizeMake(width, height);
    info.resolutionName     = J2Str(dict[@"name"]);
    info.type               = J2Str(dict[@"type"]);
    return info;
}

@end
