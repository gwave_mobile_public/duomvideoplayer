//
//  DuomPlayerModel.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/6.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DuomPlayerUrl.h"

NS_ASSUME_NONNULL_BEGIN

// 播放模式
typedef NS_ENUM(NSInteger, DuomPlayerAction) {
    PLAY_ACTION_AUTO_PLAY = 0,     //自动播放
    PLAY_ACTION_MANUAL_PLAY,   //手动播放
    PLAY_ACTION_PRELOAD        //预加载
};

@interface DuomPlayerModel : NSObject
/// 视频名称
@property (nonatomic, strong) NSString  *name;
/**
 * 直接使用URL播放
 *
 * 支持 RTMP、FLV、MP4、HLS 封装格式
 */
@property (nonatomic, strong) NSString  *videoURL;

/**
 * 多码率视频 URL
 *
 * 用于拥有多个播放地址的多清晰度视频播放
 */
@property(nonatomic, strong) NSArray<DuomPlayerUrl *> *multiVideoURLs;

/// 指定多码率情况下，默认播放的连接Index
@property(nonatomic, assign) NSUInteger defaultPlayIndex;

/// 用户自定义的封面图片URL
@property (nonatomic, strong) NSString *customCoverImageUrl;

/// 默认的封面图片URL
@property (nonatomic, strong) NSString *defaultCoverImageUrl;

/// 播放模式
@property (nonatomic, assign) DuomPlayerAction action;

/// 视频时长
@property (nonatomic, assign) NSTimeInterval duration;

/// 是否开启缓存
@property (nonatomic, assign) BOOL isEnableCache;


/// 播放配置, 为 nil 时为 "default"
@property(nonatomic, copy) NSString *pcfg;

/// 正在播放的清晰度
@property(nonatomic, copy) NSString *playingDefinition;

/// 正在播放的清晰度URL
@property(nonatomic, copy, readonly) NSString *playingDefinitionUrl;

/// 正在播放的清晰度索引
@property(nonatomic, assign, readonly) NSInteger playingDefinitionIndex;

/// 清晰度列表
@property(nonatomic, assign, readonly) NSArray *playDefinitions;

/// 视频雪碧图
//@property(nonatomic, strong) TXImageSprite *imageSprite;

/// 视频原时长（用于试看时返回完整视频时长）
@property(assign, nonatomic) NSTimeInterval originalDuration;


@end

NS_ASSUME_NONNULL_END
