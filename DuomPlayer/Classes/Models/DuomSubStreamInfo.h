//
//  DuomSubStreamInfo.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/7.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DuomSubStreamInfo : NSObject

@property(assign, nonatomic) CGSize  size;
@property(copy, nonatomic) NSString *resolutionName;
@property(copy, nonatomic) NSString *type;
+ (instancetype)infoWithDictionary:(NSDictionary *)dict;

@end

NS_ASSUME_NONNULL_END
