//
//  DuomPlayerUrl.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/6.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
/**
 * 多码率地址
 * 用于有多个播放地址的多清晰度视频的播放
 */
@interface DuomPlayerUrl : NSObject

/// 播放器展示的对应标题，如“高清”、“低清”等
@property NSString *title;
/// 播放地址
@property NSString *url;

@property (nonatomic, assign) int qualityIndex;

@end

NS_ASSUME_NONNULL_END
