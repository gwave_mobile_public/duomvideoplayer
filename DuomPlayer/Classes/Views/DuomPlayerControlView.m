//
//  DuomPlayerControlView.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/6.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomPlayerControlView.h"

@implementation DuomPlayerControlView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _compact = YES;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];

    //    UIInterfaceOrientation currentOrientation = [UIApplication sharedApplication].statusBarOrientation;
    if (self.compact) {
        [self setOrientationPortraitConstraint];
    } else {
        [self setOrientationLandscapeConstraint];
    }
    [self.delegate controlViewDidChangeScreen:self];
}

- (void)setOrientationPortraitConstraint {
}

- (void)setOrientationLandscapeConstraint {
}

- (void)resetWithResolutionNames:(NSArray<NSString *> *)resolutionNames
          currentResolutionIndex:(NSUInteger)resolutionIndex
                          isLive:(BOOL)isLive
                  isTimeShifting:(BOOL)isTimeShifting
                       isPlaying:(BOOL)isAutoPlay {
}

- (void)setPlayState:(BOOL)isPlay {
}

- (void)showOrHideBackBtn:(BOOL)isShow {
}

- (void)setSliderState:(BOOL)isEnable {
}

- (void)setTopViewState:(BOOL)isShow {
}

- (void)setResolutionViewState:(BOOL)isShow {
}

- (void)setNextBtnState:(BOOL)isShow {
}

- (void)setDisableOfflineBtn:(BOOL)disableOfflineBtn {
}

- (void)setProgressTime:(NSInteger)currentTime totalTime:(NSInteger)totalTime progressValue:(CGFloat)progress playableValue:(CGFloat)playable {
}


@end
