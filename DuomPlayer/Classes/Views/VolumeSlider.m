//
//  DuomVolumeSlider.m
//  DuomPlayer
//
//  Created by kuroky on 2022/9/9.
//

#import "VolumeSlider.h"

@implementation VolumeSlider

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self initUI];
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self initUI];
    return self;
}

- (void)initUI {
    self.transform = CGAffineTransformMakeRotation(M_PI * -0.5);
}



@end
