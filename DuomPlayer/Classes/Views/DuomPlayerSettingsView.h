//
//  DuomPlayerSettingsView.h
//  TXLiteAVDemo
//
//  Created by annidyfeng on 2018/7/4.
//  Copyright © 2018年 Tencent. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DuomPlayerViewConfig.h"

#define MoreViewWidth 330

@class DuomPlayerControlView;

@interface DuomPlayerSettingsView : UIView

@property(weak) DuomPlayerControlView *controlView;

@property UISlider *soundSlider;

@property UISlider *lightSlider;

/**
 * 是否显示播放速度和镜像
 *
 * 目前仅点播放支持修改播放速度与设置画面镜像
 */
@property(nonatomic) BOOL enableSpeedAndMirrorControl;

@property DuomPlayerViewConfig *playerConfig;
- (void)update;

@end
