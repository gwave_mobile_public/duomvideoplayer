//
//  main.m
//  DuomPlayer
//
//  Created by kuroky on 09/06/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

@import UIKit;
#import "DUOMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DUOMAppDelegate class]));
    }
}
