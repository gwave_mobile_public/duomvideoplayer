//
//  DuomNavigationViewController.h
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/8.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DuomNavigationViewController : UINavigationController

@end

NS_ASSUME_NONNULL_END
