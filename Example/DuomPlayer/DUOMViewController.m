//
//  DUOMViewController.m
//  DuomPlayer
//
//  Created by kuroky on 09/06/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

#import "DUOMViewController.h"
#import "DuomPlayerViewController.h"


@interface DUOMViewController ()


@end

@implementation DUOMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    DuomPlayerViewController *playVC = [DuomPlayerViewController new];
    [self.navigationController pushViewController:playVC animated:YES];
}


@end
