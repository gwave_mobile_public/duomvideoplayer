//
//  DUOMAppDelegate.m
//  DuomPlayer
//
//  Created by kuroky on 09/06/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

#import "DUOMAppDelegate.h"
#import "DUOMViewController.h"
#import "TXLiveBase.h"
#import "DuomNavigationViewController.h"

@interface DUOMAppDelegate()

/** 是否是横屏 */
@property (nonatomic, assign, getter=isLaunchScreen) BOOL launchScreen;

@end

@implementation DUOMAppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    DUOMViewController *vc = [[DUOMViewController alloc] init];
    DuomNavigationViewController *nav = [[DuomNavigationViewController  alloc] initWithRootViewController:vc];
    self.window.rootViewController = nav;
    self.window.backgroundColor=[UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    
    NSString *licenceURL = @"https://license.vod2.myqcloud.com/license/v2/1313175810_1/v_cube.license";
    NSString *licenceKey = @"55f35f8898b268201c5abfc8d3f5ffc7";
    [TXLiveBase setLicenceURL:licenceURL key:licenceKey];
    
    return YES;
}


- (void)setLaunchScreen:(BOOL)launchScreen {
    _launchScreen = launchScreen;
    [self application:[UIApplication sharedApplication] supportedInterfaceOrientationsForWindow:nil];
}

- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    if (self.isLaunchScreen) {
        if (@available(iOS 16, *)) {
            // 16 及以上可以做到根据屏幕方向适配横屏
            return  UIInterfaceOrientationMaskLandscape;
        } else {
            // 只支持横屏，并且 Home 按键在右边
            return UIInterfaceOrientationMaskLandscapeRight;
        }
    }
    // 只支持竖屏
    return UIInterfaceOrientationMaskPortrait;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
