//
//  DuomNavigationViewController.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/8.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomNavigationViewController.h"

@interface DuomNavigationViewController ()

@end

@implementation DuomNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (UIViewController *)childViewControllerForStatusBarHidden {
    return  self.topViewController;
}

@end
