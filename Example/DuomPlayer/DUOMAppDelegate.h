//
//  DUOMAppDelegate.h
//  DuomPlayer
//
//  Created by kuroky on 09/06/2022.
//  Copyright (c) 2022 kuroky. All rights reserved.
//

@import UIKit;

@interface DUOMAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
