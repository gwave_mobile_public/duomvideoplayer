//
//  DuomPlayerViewController.m
//  DuomPlayer_Example
//
//  Created by kuroky on 2022/9/8.
//  Copyright © 2022 kuroky. All rights reserved.
//

#import "DuomPlayerViewController.h"
#import <Masonry/Masonry.h>
#import "DuomPlayer.h"
#import <SDWebImage.h>
#import "DuomPlayerUrl.h"

static NSString *testVideoURL = @"https://webdemo.agora.io/agora-web-showcase/examples/Agora-Custom-VideoSource-Web/assets/sample.mp4";

static NSString *testVideoURL2 = @"http://1500005830.vod2.myqcloud.com/43843ec0vodtranscq1500005830/48888812387702299774211080/adp.10.m3u8";

static NSString *coverIMG = @"https://t7.baidu.com/it/u=4198287529,2774471735&fm=193&f=GIF";

@interface DuomPlayerViewController ()<DuomPlayerDelegate>

@property (nonatomic, strong) UIView *renderView;

@property (nonatomic, strong) DuomPlayerView *playerView;

@property (nonatomic, strong) DuomPlayerModel *model;

@property (nonatomic, assign) BOOL isFull;

@end

@implementation DuomPlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.isFull = NO;
    
    [self setupUI];
}

- (void)setupUI {
    [self.view addSubview:self.renderView];
    [self.renderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(400);
        make.height.mas_equalTo(400);
        make.center.equalTo(self.view);
//        make.edges.equalTo(CGRe);
    }];
    
    [self.renderView addSubview:self.playerView];
    [self.playerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.renderView);
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    DuomPlayerModel *playerModel   = [DuomPlayerModel new];
    playerModel.videoURL = testVideoURL;
    playerModel.action = PLAY_ACTION_MANUAL_PLAY;
    playerModel.name = @"test Video 🍉";
    DuomPlayerUrl *t1 = [DuomPlayerUrl new];
    t1.title = @"高清";
    t1.url = testVideoURL;
    t1.qualityIndex = 0;
    DuomPlayerUrl *t2 = [DuomPlayerUrl new];
    t2.title = @"标清";
    t2.url = testVideoURL;
    t2.qualityIndex = 1;
    playerModel.multiVideoURLs = @[t1, t2];
    self.model = playerModel;
    
    self.playerView.disablePip = NO;
    self.playerView.centerPlayBtn.hidden = NO;
    self.playerView.controlView.hidden = NO;
    self.playerView.loop = YES;
    [self.playerView showOrHideBackBtn:NO];
    [self.playerView.coverImageView sd_setImageWithURL:[NSURL URLWithString:coverIMG]];
    
    // 单个
    [self.playerView playWithModel:self.model];
    // 列表
//    [self.playerView playWithModelList:@[playerModel, playerModel, playerModel] isLoopPlayList:YES startIndex:0];
}

- (BOOL)prefersStatusBarHidden {
    return self.isFull;
}


- (UIView *)renderView {
    if (!_renderView) {
        _renderView = [[UIView alloc] initWithFrame: CGRectZero];
        _renderView.backgroundColor = [UIColor blackColor];
    }
    return _renderView;
}

- (DuomPlayerView *)playerView {
    if (!_playerView) {
        _playerView = [[DuomPlayerView alloc] initWithFrame: CGRectZero];
        _playerView.fatherView = self.renderView;
        _playerView.backgroundColor = [UIColor redColor];
        _playerView.disableGesture = NO;
        _playerView.delegate = self;
        _playerView.disableVolumControl = YES;
    }
    return _playerView;
}

- (void)dealloc {
    [self.playerView removeVideo];
}

#pragma mark - DuomPlayerDelegate
- (void)duomPlayerDidStart:(DuomPlayerView *)player {
    NSLog(@"start play");
}

- (void)duomPlayerFullScreenChanged:(DuomPlayerView *)player {
    self.isFull = player.isFullScreen;
    [self setNeedsStatusBarAppearanceUpdate];
    
    if (player.isFullScreen) {
        player.disableGesture = YES;
    } else {
        player.disableGesture = NO;
    }
}


@end
