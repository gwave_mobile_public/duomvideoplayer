# DuomPlayer

[![CI Status](https://img.shields.io/travis/kuroky/DuomPlayer.svg?style=flat)](https://travis-ci.org/kuroky/DuomPlayer)
[![Version](https://img.shields.io/cocoapods/v/DuomPlayer.svg?style=flat)](https://cocoapods.org/pods/DuomPlayer)
[![License](https://img.shields.io/cocoapods/l/DuomPlayer.svg?style=flat)](https://cocoapods.org/pods/DuomPlayer)
[![Platform](https://img.shields.io/cocoapods/p/DuomPlayer.svg?style=flat)](https://cocoapods.org/pods/DuomPlayer)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DuomPlayer is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DuomPlayer'
```

## Author

kuroky, kai.he@duom.com

## License

DuomPlayer is available under the MIT license. See the LICENSE file for more info.
